//controls the motor with arduino.  Enter number between 500-2500 into serial monitor.
//values above 1500 -> turns motor clockwise, 2500 at max speed
//values below 1500 -> turns motor counter-clockwise, 500 at max speed

#include <Servo.h>


int value = 1500; // set values you need to zero

Servo firstESC, secondESC; //Create as much as Servoobject you want. You can controll 2 or more Servos at the same time

void setup() {

  firstESC.attach(9);    // attached to pin 9 I just do this with 1 Servo
  Serial.begin(9600);    // start serial at 9600 baud

}

void loop() {

//First connect your ESC WITHOUT Arming. Then Open Serial and follo Instructions
 
  firstESC.writeMicroseconds(value);
  //Serial.println(value);
  if(Serial.available()) 
    value = Serial.parseInt();    // Parse an Integer from Serial

}

