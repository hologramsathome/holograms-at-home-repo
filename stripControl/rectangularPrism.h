#include "Arduino.h"
#include "Image.h"
#ifndef rectangularPrism_h
#define rectangularPrism_h

class RectangularPrism : public Image {
  public:
    int radius;
    //lower adn upper bounds for x,y,z
    int xLow; int xHigh;
    int yLow; int yHigh;
    int zLow; int zHigh;
    int duration;


    RectangularPrism( int x1, int x2, int y1, int y2, int z1, int z2, int c, int d);

    int getLights(int angle, int layer, int light);

    void createArr();


};

#endif
