#include "Arduino.h"
#include "Frame.h"
#ifndef Image_h
#define Image_h

class Image{
  public :
  //duration is the number of frames(rotations) the image will display for
    int duration;
    //wheel(color) any number from 0-384
    int color;

  //constructor, takes in duration and color
  Image(int d, int c);
  
  //change first arraylength based on number of divisions in frame angle!!
  //array of lights, represented by lightsArr[angle][layer][light], int representing color of displayed pixel
  //int lightsArr[8][8][12];
  //returns color for given pixel
  virtual int getLights(int angle, int layer, int light) = 0;
  //generates array
  //void createArr();

  //copies array into given array
  //void copyArr(int arr[8][8][12]);
  //unimplemented
  void displayAngle(int angle);

  Frame getFrame();

  
};
#endif
/*
class sphere : public Image{
  int radius;
  int xOffset;
  int yOffset;
  int zOffset;
  int duration;


  public : sphere(int r, int x, int y, int z, int c, int d):Image(d,c){
    radius = r;
    xOffset = x;
    yOffset = y;
    zOffset = z;
    duration = d;
    color = c;
  }
  
  int getLights(double angle, int layer, int light){
    long c = -(radius*radius-xOffset*xOffset-yOffset*yOffset-zOffset*zOffset+2*layer*zOffset);
    double b = -2*(xOffset*cos(angle)-yOffset*sin(angle));
    if(light == (int)((-b+sqrt(b*b-4*c))/2 + .5) || light == (int)((-b-sqrt(b*b-4*c))/2+.5)){
      return color;
    }
    return -1;
  }

  
};
*/

