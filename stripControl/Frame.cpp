#include "Arduino.h"
#include "Frame.h"

Frame:: Frame(int arr[8][8][12]){
  for(int a = 0; a < 8; a++){
    for(int b = 0; b < 8; b++){
      for(int c = 0; c < 12; c++){
        lightsArr[a][b][c] = arr[a][b][c];
      }
    }
  }
}

Frame:: Frame(){
  for(int a = 0; a < 8; a++){
    for(int b = 0; b < 8; b++){
      for(int c = 0; c < 12; c++){
        lightsArr[a][b][c] = -1;
      }
    }
  }
}

void Frame:: copy(Frame f){
  for(int a = 0; a < 8; a++){
    for(int b = 0; b < 8; b++){
      for(int c = 0; c < 12; c++){
        lightsArr[a][b][c] = f.getLightColor(a,b,c);
      }
    }
  }
}

void Frame:: add(Frame f){
  for(int a = 0; a < 8; a++){
    for(int b = 0; b < 8; b++){
      for(int c = 0; c < 12; c++){
        int newColor = f.getLightColor(a,b,c);
        if(newColor >-1){    
         lightsArr[a][b][c] = newColor;
        }
      }
    }
  }
}

int Frame:: getLightColor(int ang, int layer, int light){
  return lightsArr[ang][layer][light];
}

void Frame:: setLightColor(int ang, int layer, int light, int color){
  lightsArr[ang][layer][light] = color;
}

