#include "Arduino.h"
#include "Image.h"
#ifndef CenteredCylinder_h
#define CenteredCylinder_h

class CenteredCylinder : virtual public Image{
    public:
    int height;
    int radius;
    
    int zOffset;


  CenteredCylinder(int h,int r, int z, int c, int d);

  int getLights(int angle, int layer, int light);

};


#endif
