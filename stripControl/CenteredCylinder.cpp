#include "Arduino.h"
#include "CenteredCylinder.h"
#include "Image.h"

CenteredCylinder :: CenteredCylinder(int h,int r, int z, int c, int d):Image(d,c){
    height = h;
    radius = r;
    zOffset=z;
  }

  int  CenteredCylinder::getLights(int ang, int layer, int light){
    if(light <= radius && layer>=zOffset && layer<height+zOffset){
      return color;
    }
    return -1;
  }
