#include "Arduino.h"
#include "SemiCircle.h"
#include "Image.h"

  
  SemiCircle :: SemiCircle(int lay, int light, int sa, int ea, int c, int d):Image(d,c){
    layr = lay;
    startAngle = sa;
    endAngle = ea;
    lit = light;
  }
  
  int  SemiCircle::getLights(int ang, int layer, int light){
    if(layer == layr && ang >= startAngle && ang < endAngle && light <=lit && light%2==0){
      return color;
    }
    else if(layer==layr && light<lit && light%2==0){
      return color;
    }
    return -1;
  }
