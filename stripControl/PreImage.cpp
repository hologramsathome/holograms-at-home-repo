#include "Arduino.h"
#include "PreImage.h"
#include "Image.h"
#include "Cylinder.h"
#include "rectangularPrism.h"

PreImage::PreImage(String t, int p[]){
      type = t;
      paramLength = getParamLength(t);
      
      
      for(int i = 0; i < paramLength; i++){
        params[i] = p[i];
      }
    }

 int PreImage::getParamLength(String t){
  if(t.equals(SPHERE))
    return 6;
  if(t.equals(RECTANGULARPRISM))
    return 8;
  if(t.equals(CYLINDER))
    return 7;
  

}

Image* PreImage::getImage(){
      
      if(type.equals(SPHERE)){
//          Sphere point = Sphere(params[0], params[1], params[2], params[3], params[4], params[5]);
//          return &point;
      }
       if(type.equals(RECTANGULARPRISM)){
          RectangularPrism point =  RectangularPrism(params[0], params[1], params[2], params[3], params[4], params[5],params[6],params[7]);
          return &point;
       }
       if(type.equals(CYLINDER)){
          Cylinder point =  Cylinder(params[0], params[1], params[2], params[3], params[4], params[5],params[6]);
          return &point;
       }
    }
