#include "Arduino.h"
#include "Image.h"
#ifndef PreImage_h
#define PreImage_h

class PreImage{
  public:
    String type;
    int params[10];
    int paramLength;

     const String SPHERE = "Sphere";
     const String RECTANGULARPRISM = "RectangularPrism";
     const String CYLINDER = "Cylinder";

    PreImage(String t, int p[]);

    Image* getImage();

    int getParamLength(String t);

};

#endif

