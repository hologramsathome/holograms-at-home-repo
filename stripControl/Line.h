#include "Arduino.h"
#include "Image.h"
#ifndef Line_h
#define Line_h

class Line : virtual public Image{
    public:
    int layr;
    int angle;


  Line(int a, int la, int c, int d);

  int getLights(int angle, int layer, int light);

};


#endif
