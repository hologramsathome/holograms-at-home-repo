#include "Arduino.h"
#include "Image.h"

  Image :: Image(int d, int c){
    duration = d;
    color = c;
  }
  //change first arraylength based on number of divisions in frame angle!!

/*
  int Image::getLights(int angle, int layer, int light){
    return -1;
  }
  */
  /*
  void Image:: createArr(){
    for(int a = 0; a < sizeof(lightsArr)/sizeof(lightsArr[0]); a++){
      for(int la = 0; la < sizeof(lightsArr[0])/sizeof(lightsArr[0][0]); la++){
        for(int li = 0; li<sizeof(lightsArr[0][0])/sizeof(lightsArr[0][0][0]);li++){
          lightsArr[a][la][li] = getLights(a,la,li);
        }
      }
    }
  }
  
  void Image:: copyArr(int arr[8][8][12]){
    for(int a = 0; a < sizeof(lightsArr)/sizeof(lightsArr[0]); a++){
      for(int la = 0; la < sizeof(lightsArr[0])/sizeof(lightsArr[0][0]); la++){
        for(int li = 0; li<sizeof(lightsArr[0][0])/sizeof(lightsArr[0][0][0]);li++){
          arr[a][la][li] = lightsArr[a][la][li];
        }
      }
    }
  }
  
  void Image:: displayAngle(int angle){
    for(int la = 0; la < sizeof(lightsArr[0])/sizeof(lightsArr[0][0]); la++){
      for(int li = 0; li < sizeof(lightsArr[0][0])/sizeof(lightsArr[0][0][0]); li++){
        //setLights(la,li,lightsArr[angle][la][li]);
      }
    }
  }
  */

  Frame Image:: getFrame(){
    int lightsArr[8][8][12];
    for(int a = 0; a < sizeof(lightsArr)/sizeof(lightsArr[0]); a++){
      for(int la = 0; la < sizeof(lightsArr[0])/sizeof(lightsArr[0][0]); la++){
        for(int li = 0; li<sizeof(lightsArr[0][0])/sizeof(lightsArr[0][0][0]);li++){
          lightsArr[a][la][li] = getLights(a,la,li);
        }
      }
    }
    return Frame(lightsArr);
  }


/*
class sphere : public Image{
  int radius;
  int xOffset;
  int yOffset;
  int zOffset;
  int duration;


  public : sphere(int r, int x, int y, int z, int c, int d):Image(d,c){
    radius = r;
    xOffset = x;
    yOffset = y;
    zOffset = z;
    duration = d;
    color = c;
  }
  
  int getLights(double angle, int layer, int light){
    long c = -(radius*radius-xOffset*xOffset-yOffset*yOffset-zOffset*zOffset+2*layer*zOffset);
    double b = -2*(xOffset*cos(angle)-yOffset*sin(angle));
    if(light == (int)((-b+sqrt(b*b-4*c))/2 + .5) || light == (int)((-b-sqrt(b*b-4*c))/2+.5)){
      return color;
    }
    return -1;
  }

  
};
*/

