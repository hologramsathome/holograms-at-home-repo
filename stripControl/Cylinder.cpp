#include "Arduino.h"
#include "Cylinder.h"
#include "Image.h"

  Cylinder :: Cylinder(int h,int r, int x, int y, int z, int c, int d):Image(d,c){
    height = h;
    radius = r;
    int xOffset=x;
    int yOffset=y;
    int zOffset=z;
  }

  int  Cylinder::getLights(int ang, int layer, int light){
    double angle = ang*(3.14/4) + layer*3.14/4+(light%2*3.14);
    long c = radius*radius-xOffset*xOffset-yOffset*yOffset;
    double b = (xOffset*cos(angle)+yOffset*sin(angle));
    if(light == (int)((b+sqrt(b*b+c)) + .5) || light == (int)((b+sqrt(b*b+c))+.5) ){
      if(zOffset+height>=layer){
        return color;
      }
    }
    return -1;
  }
