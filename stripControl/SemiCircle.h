#include "Arduino.h"
#include "Image.h"
#ifndef SemiCircle_h
#define SemiCircle_h

class SemiCircle : virtual public Image{
    public:
    int layr;
    int lit;
    int startAngle;
    int endAngle;
    

  SemiCircle(int lay,int light, int sa, int ea, int c, int d);

  int getLights(int angle, int layer, int light);

};

#endif

