//code for the Arduino Mega.  Controls switching LED lights, along with interpreting data from hall effect sensor

#include "LPD8806.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#include "Image.h"
#include "Cylinder.h"
#include "CenteredCylinder.h"
#include "rectangularPrism.h"
#include "CenteredSphere.h"
#include "Line.h"
#include "SemiCircle.h"
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

// Example to control LPD8806-based RGB LED Modules in a strip

/*****************************************************************************/

bool check = true;

int line=0;

unsigned long lastDetected;


Frame currentFrame;
int frameNum;


  CenteredCylinder c0 = CenteredCylinder(8,0,0,200,1);
  CenteredCylinder c1 = CenteredCylinder(8,1,0,200,1);
  CenteredCylinder c2 = CenteredCylinder(8,2,0,200,1);
  CenteredCylinder c3 = CenteredCylinder(8,3,0,200,1);
  CenteredCylinder c4 = CenteredCylinder(8,4,0,200,1);
  CenteredCylinder c5 = CenteredCylinder(8,5,0,200,1);
  CenteredCylinder c6 = CenteredCylinder(8,6,0,200,1);
  CenteredCylinder c7 = CenteredCylinder(8,7,0,200,1);
  CenteredCylinder c8 = CenteredCylinder(8,8,0,200,1);
  CenteredCylinder c9 = CenteredCylinder(8,9,0,200,1);
  CenteredCylinder c10 = CenteredCylinder(8,10,0,200,1);
  CenteredCylinder c11= CenteredCylinder(8,11,0,200,1);

  CenteredCylinder s0 = CenteredCylinder(1,10,0,0,1);
  CenteredCylinder s1 = CenteredCylinder(1,10,1,25,1);
  CenteredCylinder s2 = CenteredCylinder(1,10,2,50,1);
  CenteredCylinder s3 = CenteredCylinder(1,10,3,75,1);
  CenteredCylinder s4 = CenteredCylinder(1,10,4,100,1);
  CenteredCylinder s5 = CenteredCylinder(1,10,5,125,1);
  CenteredCylinder s6 = CenteredCylinder(1,10,6,150,1);
  CenteredCylinder s7 = CenteredCylinder(1,10,7,175,1);

  Line l0 = Line(0,7,200,3);
  Line l1 = Line(1,7,200,3);
  Line l2 = Line(2,7,200,3);
  Line l3 = Line(3,7,200,3);
  Line l4 = Line(4,7,200,3);
  Line l5 = Line(5,7,200,3);
  Line l6 = Line(6,7,200,3);
  Line l7 = Line(7,7,200,3);

  Line l00 = Line(0,0,200,3);
  Line l01 = Line(0,1,200,3);
  Line l02 = Line(0,2,200,3);
  Line l03 = Line(0,3,200,3);
  Line l04 = Line(0,4,200,3);
  Line l05 = Line(0,5,200,3);
  Line l06 = Line(0,6,200,3);
  Line l07 = Line(0,7,200,3);

  SemiCircle sc00 = SemiCircle(7, 0, 0, 1, 100, 3);
  SemiCircle sc01 = SemiCircle(7, 0, 0, 2, 100, 3);
  SemiCircle sc02 = SemiCircle(7, 0, 0, 3, 100, 3);
  SemiCircle sc03 = SemiCircle(7, 0, 0, 4, 100, 3);
  SemiCircle sc04 = SemiCircle(7, 0, 0, 5, 100, 3);
  SemiCircle sc05 = SemiCircle(7, 0, 0, 6, 100, 3);
  SemiCircle sc06 = SemiCircle(7, 0, 0, 7, 100, 3);
  SemiCircle sc07 = SemiCircle(7, 0, 0, 8, 100, 3);
  
  SemiCircle sc10 = SemiCircle(7, 2, 0, 1, 100, 3);
  SemiCircle sc11 = SemiCircle(7, 2, 0, 2, 100, 3);
  SemiCircle sc12 = SemiCircle(7, 2, 0, 3, 100, 3);
  SemiCircle sc13 = SemiCircle(7, 2, 0, 4, 100, 3);
  SemiCircle sc14 = SemiCircle(7, 2, 0, 5, 100, 3);
  SemiCircle sc15 = SemiCircle(7, 2, 0, 6, 100, 3);
  SemiCircle sc16 = SemiCircle(7, 2, 0, 7, 100, 3);
  SemiCircle sc17 = SemiCircle(7, 2, 0, 8, 100, 3);

  SemiCircle sc20 = SemiCircle(7, 4, 0, 1, 100, 3);
  SemiCircle sc21 = SemiCircle(7, 4, 0, 2, 100, 3);
  SemiCircle sc22 = SemiCircle(7, 4, 0, 3, 100, 3);
  SemiCircle sc23 = SemiCircle(7, 4, 0, 4, 100, 3);
  SemiCircle sc24 = SemiCircle(7, 4, 0, 5, 100, 3);
  SemiCircle sc25 = SemiCircle(7, 4, 0, 6, 100, 3);
  SemiCircle sc26 = SemiCircle(7, 4, 0, 7, 100, 3);
  SemiCircle sc27 = SemiCircle(7, 4, 0, 8, 100, 3);

  SemiCircle sc30 = SemiCircle(7, 6, 0, 1, 100, 3);
  SemiCircle sc31 = SemiCircle(7, 6, 0, 2, 100, 3);
  SemiCircle sc32 = SemiCircle(7, 6, 0, 3, 100, 3);
  SemiCircle sc33 = SemiCircle(7, 6, 0, 4, 100, 3);
  SemiCircle sc34 = SemiCircle(7, 6, 0, 5, 100, 3);
  SemiCircle sc35 = SemiCircle(7, 6, 0, 6, 100, 3);
  SemiCircle sc36 = SemiCircle(7, 6, 0, 7, 100, 3);
  SemiCircle sc37 = SemiCircle(7, 6, 0, 8, 100, 3);

  SemiCircle sc40 = SemiCircle(7, 8, 0, 1, 100, 3);
  SemiCircle sc41 = SemiCircle(7, 8, 0, 2, 100, 3);
  SemiCircle sc42 = SemiCircle(7, 8, 0, 3, 100, 3);
  SemiCircle sc43 = SemiCircle(7, 8, 0, 4, 100, 3);
  SemiCircle sc44 = SemiCircle(7, 8, 0, 5, 100, 3);
  SemiCircle sc45 = SemiCircle(7, 8, 0, 6, 100, 3);
  SemiCircle sc46 = SemiCircle(7, 8, 0, 7, 100, 3);
  SemiCircle sc47 = SemiCircle(7, 8, 0, 8, 100, 3);

 // Line calibrate = Line(0,7,100,50,    0);
  

  RectangularPrism rp0 = RectangularPrism(-4, 4, -4, 4, 0, 8, 200, 500);
  

  

Image* frames[300] = {
  &sc00, &sc01, &sc02, &sc03, &sc04, &sc05, &sc06, &sc07,
  &sc10, &sc11, &sc12, &sc13, &sc14, &sc15, &sc16, &sc17,
  &sc20, &sc21, &sc22, &sc23, &sc24, &sc25, &sc26, &sc27,
  &sc30, &sc31, &sc32, &sc33, &sc34, &sc35, &sc36, &sc37,
  &sc40, &sc41, &sc42, &sc43, &sc44, &sc45, &sc46, &sc47,
  
  &s0,
  &s1,
  &s2,
  &s3,
  &s4,
  &s5,
  &s6,
  &s7,
  &s0,
  &s1,
  &s2,
  &s3,
  &s4,
  &s5,
  &s6,
  &s7,
  &s0,
  &s1,
  &s2,
  &s3,
  &s4,
  &s5,
  &s6,
  &s7,
  
  &l0,
  &l1,
  &l2,
  &l3,
  &l4,
  &l5,
  &l6,
  &l7,
  &l0,
  &l1,
  &l2,
  &l3,
  &l4,
  &l5,
  &l6,
  &l7,
  &l0,
  &l1,
  &l2,
  &l3,
  &l4,
  &l5,
  &l6,
  &l7,
  
  &c0,
  &c1,
  &c2,
  &c3,
  &c4,
  &c5,
  &c6,
  &c7,
  &c8,
  &c9,
  &c10,
  &c11,
  &c10,
  &c9,
  &c8,
  &c7,
  &c6,
  &c5,
  &c4,
  &c3,
  &c2,
  &c1,
  &c0,
  &c0,
  &c1,
  &c2,
  &c3,
  &c4,
  &c5,
  &c6,
  &c7,
  &c8,
  &c9,
  &c10,
  &c11,
  &c10,
  &c9,
  &c8,
  &c7,
  &c6,
  &c5,
  &c4,
  &c3,
  &c2,
  &c1,
  &c0,
  &c0,
  &c1,
  &c2,
  &c3,
  &c4,
  &c5,
  &c6,
  &c7,
  &c8,
  &c9,
  &c10,
  &c11,
  &c10,
  &c9,
  &c8,
  &c7,
  &c6,
  &c5,
  &c4,
  &c3,
  &c2,
  &c1,
  &c0,

  &l00,
  &l01,
  &l02,
  &l03,
  &l04,
  &l05,
  &l06,
  &l07,
  &l06,
  &l05,
  &l04,
  &l03,
  &l02,
  &l01,
  &l00,
  &l00,
  &l01,
  &l02,
  &l03,
  &l04,
  &l05,
  &l06,
  &l07,
  &l06,
  &l05,
  &l04,
  &l03,
  &l02,
  &l01,
  &l00,
  &l00,
  &l01,
  &l02,
  &l03,
  &l04,
  &l05,
  &l06,
  &l07,
  &l06,
  &l05,
  &l04,
  &l03,
  &l02,
  &l01,
  &l00,

};

int frameArrLength = 202;

//tot 186

//int a[6] = {3,0,0,4,100,10};
//int b[6] = {4,0,0,4,200,10};
//PreImage frames[2] = {
//  PreImage(SPHERE,a),
//  PreImage(SPHERE,b)
//};

// Number of RGB LEDs in strand:
int nLEDs = 48;

//time in milliseconds
unsigned long period = 200000;
//1630 on motor
int division = 8;
unsigned long rotStart = 0;

unsigned long frameCount = 0;

int currentAngle = 0;

unsigned long endtime1, endtime2, starttime;

// Chose 2 pins for output; can be any valid output pins:
int dataPin1  = 51;
int clockPin1 = 52;

int dataPin2 = 41;
int clockPin2 = 39;

// First parameter is the number of LEDs in the strand.  The LED strips
// are 32 LEDs per meter but you can extend or cut the strip.  Next two
// parameters are SPI data and clock pins:
LPD8806 strip1 = LPD8806(nLEDs);
LPD8806 strip2 = LPD8806(nLEDs, dataPin2, clockPin2);

 // Image s1 = RectangularPrism(-3,  3,  -3,  3, 0, 3, 100, 100);
 // Image * s1;

// You can optionally use hardware SPI for faster writes, just leave out
// the data and clock pin parameters.  But this does limit use to very
// specific pins on the Arduino.  For "classic" Arduinos (Uno, Duemilanove,
// etc.), data = pin 11, clock = pin 13.  For Arduino Mega, data = pin 51,
// clock = pin 52.  For 32u4 Breakout Board+ and Teensy, data = pin B2,
// clock = pin B1.  For Leonardo, this can ONLY be done on the ICSP pins.
//LPD8806 strip = LPD8806(nLEDs);

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif
//set up pin 2 for interrupt: call magnet_detect() when signal goes from 0 to 1
attachInterrupt(digitalPinToInterrupt(3), magnet_detect,RISING);

Serial.begin(9600);
  // Start up the LED strip
  strip1.begin();
  strip2.begin();

  // Update the strip, to start they are all 'off'
  strip1.show();
  strip2.show();
  Serial.println("begin");
  frameNum = 0;


////  RectangularPrism point = RectangularPrism(-3,3,-3,3,0,4,100,10);
//  Sphere* point = new Sphere(3,0,0,3,100,100);
//  s1 = point;
//  //s1 = frames[0].getImage();
//  s1->createArr();

  loadFrame(frameNum);
  //displayHi(500);
  //displayVertLine(0,500);
  //displayHelix(11,500);
  //displayLine(0,7,200,500);
}

void loadFrame(int c){
  currentFrame.copy(frames[c]->getFrame());
  frameCount = frames[c]->duration;
}
//do this when magnet is detected
void magnet_detect(){
  //reset current angle
  currentAngle = 0;
  //calculate period
  unsigned long currentTime = micros();
  unsigned long temp = (currentTime-lastDetected);
  Serial.println("DETECTED");
  //if(temp > 50000){
    period = temp;
    frameCount--;
    lastDetected = currentTime;
    
   Serial.print("detected: ");
    Serial.println(period);
  //}
  //add to frameCount
  
}


void loop() {

    if(frameCount <= 0){
      frameNum++;
      if(frameNum >= frameArrLength){
        frameNum = 0;
      }
      loadFrame(frameNum);
    }

    
    unsigned long interval = period/division;


    unsigned long endTime = rotStart + (currentAngle+1)*interval;
    unsigned long currentTime = micros();

//    Serial.print("current time: ");
//    Serial.println(currentTime);

    if(currentTime > endTime){
//      Serial.print("current:");
//      Serial.println(currentTime);
//      Serial.print("compare:");
//      Serial.println(endTime);
      currentAngle = (currentTime-rotStart)/interval;
      if(currentAngle >=division){
        currentAngle = 0;
        rotStart = currentTime;
        frameCount--;
      }
      displayAngle(currentAngle);
      
      //Serial.println(frameNum);
    }


      

}

void displayOffAngle(int a){
  for(int la = 0; la < 8; la++){
    for(int li = 0 ; li < 12; li++){
      setLight(la,li,-1);
    }
  }
}

void displayAngle(int a){
  for(int la = 0; la < 8; la++){
    for(int li = 0 ; li < 12; li++){
      setLight(la,li,currentFrame.getLightColor(a,la,li));
    }
  }
  
  strip1.show();
  strip2.show();
  
}

void allOff(){
    for(int la = 0; la < 8; la++){
      for(int li = 0; li < 12; li++){
        setLight(la,li,-1);
      }
    }
  
}


void setLight(int layer, int light, int color){
   if (light % 2 == 1) { //odd then strip1
    strip1.setPixelColor((layer) * 6 + ((light) / 2) , Wheel(color));
//    strip1.show();
  }

  else if (light%2 == 0) { //even then strip2
    strip2.setPixelColor((layer) * 6 + ((light) / 2), Wheel(color));
 //   strip2.show();
  }
}

  

void showAll(){
  strip1.show();
  strip2.show();
}

/* Helper functions */

//Input a value 0 to 384 to get a color value.
//The colours are a transition r - g -b - back to r

uint32_t Wheel(int WheelPos)
{
  byte r, g, b;
  if(WheelPos <0){
    return 0;
  }
  switch(WheelPos / 128)
  {
    case 0:
      r = 127 - WheelPos % 128;   //Red down
      g = WheelPos % 128;      // Green up
      b = 0;                  //blue off
      break; 
    case 1:
      g = 127 - WheelPos % 128;  //green down
      b = WheelPos % 128;      //blue up
      r = 0;                  //red off
      break; 
    case 2:
      b = 127 - WheelPos % 128;  //blue down 
      r = WheelPos % 128;      //red up
      g = 0;                  //green off
      break; 
  }
  return(strip1.Color(r,g,b));
}


