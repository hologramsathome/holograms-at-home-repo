#include "Arduino.h"
#include "Image.h"
#ifndef Cylinder_h
#define Cylinder_h

class Cylinder : virtual public Image{
    public:
    int height;
    int radius;
    
    int xOffset;
    int yOffset;
    int zOffset;


  Cylinder(int h,int r, int x, int y, int z, int c, int d);

  int getLights(int angle, int layer, int light);

};

#endif

