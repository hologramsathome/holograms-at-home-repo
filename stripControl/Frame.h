#include "Arduino.h"
#ifndef Frame_h
#define Frame_h

class Frame{
  public:
    int lightsArr[8][8][12];
    void setLights(int lights[8][8][12]);
    Frame(int lights[8][8][12]);
    Frame();
    int getLightColor(int ang, int layer, int light);

    void copy(Frame f);

    void add(Frame f);

    void setLightColor(int ang, int layer, int light, int color);
};

#endif
