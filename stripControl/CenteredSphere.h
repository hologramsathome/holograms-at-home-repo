#include "Arduino.h"
#include "Image.h"
#ifndef CenteredSphere_h
#define CenteredSphere_h

class CenteredSphere : virtual public Image{
    public:
    int height;
    int radius;
    
    int zOffset;


  CenteredSphere(int r, int z, int c, int d);

  int getLights(int angle, int layer, int light);

};

#endif
